@extends('layouts.app')

@section('content')
<div class="container">

      <div class="row justify-content-center">
              <div class="col-md-6">


                    <form>
  <div class="form-group">
    <label for="exampleFormControlInput1">Clave</label>
    <input type="text" class="form-control" id="exampleFormControlInput" >
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Nombre</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" >
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Estado</label>
    <input type="text" class="form-control" id="exampleFormControlInput1" >
  </div>
  
  <div class="form-group">
    <label for="exampleFormControlSelect2">Mes</label>
    <select class="form-control" id="exampleFormControlSelect2">
    <option value="0">Todos</option>
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Septiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect2">Año</label>
    <select class="form-control" id="exampleFormControlSelect2">
    <option value="2019">2019</option>
    <option value="2020">2020</option>
    <option value="2021">2021</option>
    </select>
  </div>


  <button type="submit" class="btn btn-primary">Enviar</button>
  
</form>




              </div>
      </div>
</div>
@endsection