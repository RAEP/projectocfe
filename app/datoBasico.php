<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datoBasico extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'clave', 'nombre', 'estado','mes','year',
    ];
}
