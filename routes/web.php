<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('pruebas',function (){

    return view('prueba');
});

Route::get('reporte1',function (){

    return view('Reportes/reporte1');
});

Route::get('reporte2',function (){

    return view('Reportes/reporte2');
});

Route::get('reporte3',function (){

    return view('Reportes/reporte3');
});

Route::get('datoBasico',function (){

    return view('datoBasico');
});