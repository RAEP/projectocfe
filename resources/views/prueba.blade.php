@extends('layouts.app')

@section('content')
<div class="container">

      <div class="row justify-content-center">
              <div class="col-md-8">
                    <div class="card-deck">

                            <div class="card">
                                  <img src="img/analitica.png" class="card-img-top" alt="...">
                                  <div class="card-body">
                                      <h5 class="card-title">Reporte 1</h5>
                                      <p class="card-text"></p>
                                      <a href="{{url('/reporte1') }}" class="btn btn-primary">Go </a>
                                  </div>
                            </div>

                            <div class="card">
                                  <img src="img/analitica.png" class="card-img-top" alt="...">
                                  <div class="card-body">
                                      <h5 class="card-title">Reporte 2</h5>
                                      <p class="card-text"></p>
                                      <a href="{{url('/reporte2')}}" class="btn btn-primary">Go </a>
                                  </div>
                            </div>

                            <div class="card">
                                  <img src="img/analitica.png" class="card-img-top" alt="...">
                                  <div class="card-body">
                                      <h5 class="card-title">Reporte 3</h5>
                                      <p class="card-text"></p>
                                      <!-- <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p> -->
                                      <a href="{{url('/reporte3')}}" class="btn btn-primary">Go </a>
                                  </div>
                            </div>

                    </div>                
              </div>
      </div>
</div>
@endsection