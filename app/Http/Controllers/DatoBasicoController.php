<?php

namespace App\Http\Controllers;

use App\datoBasico;
use Illuminate\Http\Request;

class DatoBasicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\datoBasico  $datoBasico
     * @return \Illuminate\Http\Response
     */
    public function show(datoBasico $datoBasico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\datoBasico  $datoBasico
     * @return \Illuminate\Http\Response
     */
    public function edit(datoBasico $datoBasico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\datoBasico  $datoBasico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, datoBasico $datoBasico)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\datoBasico  $datoBasico
     * @return \Illuminate\Http\Response
     */
    public function destroy(datoBasico $datoBasico)
    {
        //
    }
}
